struct Pair {
    string first;  // что заменить
    string second; // на что заменить
}

inline string find_classname_in_line(string line){
    var words = line.split(" ");
    for (int i = 0; i < words.length; i++) {
        //print(@"$(words[i]) \n");
        if (words[i] == "class" || words[i] == "struct" ) return words[i+1];
       
    }
    return "cant find class or struct name";
}
//TODO добавить проверку на то что класс не статически
//TODO придумать что нужно отличать строчки вызовов статических функций класса от создания переменных, помойму достаточно = в линии, но не ==
inline string[] fill_class_names( ref string[] lines){
    string[] class_names = {};
    foreach (var line in lines) {
        if ("class" in line || "struct" in line) {
            //если наследуется
            if(!(":" in line)){
                class_names += find_classname_in_line(line);
            }
            //если не наследуется
            else{
                var two_parts = line.split(":");
                assert (two_parts.length == 2);
                //var words = two_parts[0].split(" ");
                //message(two_parts[0]);
                class_names += find_classname_in_line(two_parts[0]);                
            }
        }
    }
    return class_names;
}

//  int[] find_where_new_keyworlds_needed(){

//   }


static int main(string[] args) {
    Pair[] pairs = {};
    string content;
    string file_name = "test.vala";
    FileUtils.get_contents(file_name, out content);
    //  print(content + "\n");

    string[] lines = content.split("\n");
    string[] class_names = fill_class_names(ref lines);
    int[] num_lines_where_replace_needed = {};
    string[] lines_where_replace_needed = {};
    int line_number = 0;


    var in_file  = FileStream.open(file_name, "r");
    var out_file = FileStream.open("new_" + file_name, "w");
    bool match_flag = false;
	string line = in_file.read_line();
	while (line != null){
        ++line_number;
        debug("current line\t%s\n", line); 

        match_flag = false;
        foreach (var klass in class_names) { // подходит строка или нет
            //print(@"\t for class $klass $(!("class" in line)) && $("=" in line) && $(klass in line)\n");// если буква в хайкейсе равна самой себе то она большая
            // old if (!("class" in line) && "=" in line && klass in line) match_flag = true;
            // подумать над тем будет ли это срабатывать на делегатах
            if ("=" in line && klass in line){
                var split = line.split("="); assert (split.length==2);
                if (split[1][0:1].up() == split[1][0:1]) match_flag = true; // первая буква большая
            }
        }
        message("Check line : " + line + @"$match_flag");
            if (match_flag)
            {
                //получается в num lines и lines хранятся номера линий и соответствующии им линии, шоб потом заменить в общем массиве линий эти линии по соответствующим индексам -1
                num_lines_where_replace_needed += line_number; 
                lines_where_replace_needed += line; 
    
                warning("replace need on " + line ); 
                //замена
                var equ_split = line.split("="); 
                assert (equ_split.length == 2);
                equ_split[1] = " = new " + equ_split[1];
                string real_replace = equ_split[0] + equ_split[1];
                //lines[num_replace_line-1] = real_replace;
                out_file.puts(real_replace +"\n");
            }
            else out_file.puts(line + "\n");
        line = in_file.read_line();
        
    }

    return 0;
}

    //  //== нет в линии, = есть в линии, слова класс нет в линии
    //  foreach (var line in lines) {
    //      ++line_number;
    //      for (int i = 0; i < class_names.length; i++) {
    //          if (!("class" in line) && !("==" in line)
    //              && "=" in line && class_names[i] in line){
    //                  //получается в num lines и lines хранятся номера линий и соответствующии им линии, шоб потом заменить в общем массиве линий эти линии по соответствующим индексам -1
    //                  num_lines_where_replace_needed += line_number;
    //                  lines_where_replace_needed += line;
    //                  message("replace need" + line );
                   

    //              }
    //      }
    //  }
    //  foreach (var num_replace_line in num_lines_where_replace_needed) {
    //      message(@"num_replace_line = $num_replace_line");
    //      //message(@"lines_where.. = $(lines_where_replace_needed[num_replace_line-1])");
    //      var equ_split = lines[num_replace_line-1].split("=");
    //      assert (equ_split.length == 2);
    //      equ_split[1] = " = new " + equ_split[1];
    //      string real_replace = equ_split[0] + equ_split[1];
    //      lines[num_replace_line-1] = real_replace;
    //  }

    //  foreach (var line in lines) {
    //      stdout.printf (line + "\n");
    //  }

    //  foreach (var a in lines_where_replace_needed) {
    //      message("replase needed in " + a.to_string());
    //  }

    //  //проверка
    //  foreach (var class_name in class_names) {
    //      message(class_name);
    //  }
