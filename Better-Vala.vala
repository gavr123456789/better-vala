using Verbex;
using Gee;
int add(){return 5;}


[Compact]
class BasicTypes {
    public BasicTypes(){
        BasicTypes = new ArrayList<string>.wrap({"int","string"});
    }
    public ArrayList<string> BasicTypes;
}

class BetterVala : GLib.Object { 
    public string content; // content of file
    public BetterVala(string filename) {
        try {
            FileUtils.get_contents(filename, out content);
        } catch (Error e){
            error("%s", e.message);
        }
        
        
    }

    /**
     * Error 1
     * func(int a);
     * {sdsd
     * 
     * }
     */
    public void infix_funcs (){
        var verbex = new VerbalExpression()
        // func  ()   {
        //}
        .anything()//func name and spaces before (
        .then("(")
        .something_but(",")//
        .then(")")
        .something_but(";") // Error 1 fix
        .then("{");
        
        foreach (var line in content.split("\n")) {
            if (verbex.matches (line)){
                prin(line + " Успех");
            }
        }
        //verbex.add_modifier('s');
    }
}

public static int main(string[] args) {
    var a = new BetterVala("Better-Vala.vala");
    a.infix_funcs();
    return 0;
}