using Gee;
//Compile valac gee_lib_test.vala  Gee_fast_structs.vala --pkg gee-0.8
namespace fast_structs_gee { 

    void parsing_brackets (ref string str){
        if(str.contains("[") || str.contains("(") || str.contains("{")){
            str._delimit("{}[]()",'\\');
            str = str.replace("\\","");
        }
    }

    void parsing(ref string str, ref string[] arr){
        parsing_brackets(ref str);

        if(str.contains(",") && !str.contains(" "))
            arr = str.split(",");
        else if (str.contains(" ") && !str.contains(","))
            arr = str.split(" ");
        else if (str.contains(" ") && str.contains(",")){
            str = str.replace(" ","");
            arr = str.split(",");
        }
    }

    void parsing_string(ref string str, ref string[] arr){
        parsing_brackets(ref str);

        if(str.contains(","))
            arr = str.split(",");
        else if (str.contains(" ") && !str.contains(","))
            arr = str.split(" ");
    }
    //of(1..10)
    int[] of(string str){
        int[] result ={};
        int start = 0;
        int end = 0;
        if (".." in str){
            start = int.parse(str.split("..")[0]);
            end   = int.parse(str.split("..")[1]);
            for (int a = start; a <= end; a++) {
                result += a;
            }
        }
        return result;
    }
    // THERE NO G.PARSE ((
    //  ArrayList<G> listof<G>(owned string str){
    //      string[] arr = {};
    //      parsing(ref str, ref arr);

    //      var list = new ArrayList<G>();
    //      foreach (var item in arr) 
    //          list.add(G.parse(item));
    //      return list;
    //  }

    ArrayList<int> li(owned string str){
        string[] arr = {};
        parsing(ref str, ref arr);

        var list = new ArrayList<int>();
        foreach (var item in arr) 
            list.add(int.parse(item));
        return list;
    }

    ArrayList<string> ls(owned string str){
        var list = new ArrayList<string>();
        string[] arr = {};
        parsing_string(ref str, ref arr);
        //adding
        foreach (var item in arr) 
            list.add(item);
        return list;
    }

    ArrayList<bool> lb(owned string str){
        var list = new ArrayList<bool>();
        string[] arr = {};
        parsing(ref str, ref arr);
        foreach (var item in arr) 
            list.add(bool.parse(item));
        return list;
    }

    ArrayList<double?> ld(owned string str){
        var list = new ArrayList<double?>();
        string[] arr = {};
        parsing(ref str, ref arr);
        foreach (var item in arr) 
            list.add(double.parse(item));
        return list;
    }

    HashMap<string,int> msi(owned string str){
        string[] arr = {};
        parsing(ref str, ref arr);
        var map = new HashMap<string,int>();

        foreach (var a in arr) {
            string[] temparr = a.split(":"); assert (temparr.length == 2);
            map[temparr[0]] = int.parse(temparr[1]);
        }
        return map;
    }

    HashMap<string,string> mss(owned string str){
        string[] arr = {};
        parsing(ref str, ref arr);
        var map = new HashMap<string,string>();

        foreach (var a in arr) {
            string[] temparr = a.split(":"); assert (temparr.length == 2);
            map[temparr[0]] = temparr[1];
        }
        return map;
    }

}