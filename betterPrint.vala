using Gee;
void prin(Variant str)  { stdout.printf(basic_to_string2(str) + "\n");}
void pringee(Value str) { stdout.printf(basic_to_string(str) + "\n");}

string basic_to_string (Value val){
    //  var typeName = data.type_name(); //message(typeName);
    string res = "";
    
    if (val.type().is_a (typeof (Gee.Traversable))) {
        if ( (val as Gee.Traversable).element_type == typeof (int)) res = ai_deserialization    ((val as Collection<int>).to_array());    else
        if ( (val as Gee.Traversable).element_type == typeof (string)) res = as_deserialization ((val as Collection<string>).to_array()); else        
        if ( (val as Gee.Traversable).element_type == typeof (bool)) res = ab_deserialization   ((val as Collection<bool>).to_array());   else        
        if ( (val as Gee.Traversable).element_type == typeof (double)) res = ad_deserialization ((val as Collection<double>).to_array());         
        //res = ai_deserialization ((val as Gee.Traversable.element_type).to_array());             message(res);
    } else

	if (val.type().is_a (typeof (int)))    {res = @"$((int)val)";}    else
    if (val.type().is_a (typeof (string))) {res = (string)val;}       else
    if (val.type().is_a (typeof (bool)))   {res = @"$((bool)val)";}   else
    if (val.type().is_a (typeof (double))) {res = @"$((double)val)";} else
    if (val.type().is_a (typeof (string[]))) {
        string[] arr = (string[])val;
        res = as_deserialization(arr);
    } 

	return res;
}

string basic_to_string2 (Variant v){
    string res = "";
    //message(v.get_type().peek_string());
    //message(v.get_type_string());
    
    if (v.get_type().is_basic()){//message("basic");
        if (v.get_type_string() == "i") {res = @"$((int)v)";}    else
        if (v.get_type_string() == "d") {res = @"$((double)v)";} else
        if (v.get_type_string() == "b") {res = @"$((bool)v)";}   else
        if (v.get_type_string() == "s") {res = (string)v;} 
    }
    if (v.get_type().is_array()) {//message("array");
        if (v.get_type_string() == "ai") {res = ai_deserialization((int[])v);}    else
        if (v.get_type_string() == "as") {res = as_deserialization((string[])v);} else
        if (v.get_type_string() == "ab") {res = ab_deserialization((bool[])v);}   else
        if (v.get_type_string() == "ad") {res = ad_deserialization((double[])v);} 
    }
	return res;
}

string bracing(string s){ return "{" + s + "}"; }
//TODO: Temp solution until https://gitlab.gnome.org/GNOME/vala/issues/834
string ai_deserialization(int[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
string ab_deserialization(bool[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");                                         //message(strBuilder.str);
    return bracing(strBuilder.str);
}
string ad_deserialization(double[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");                                         //message(strBuilder.str);
    return bracing(strBuilder.str);
}
string as_deserialization(string[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) {
        strBuilder.append(arr[i]); 
        strBuilder.append(", ");
    }
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
